﻿using System;
using System.Net.Configuration;

namespace tqbot
{
    public static class Log
    {
        private static readonly object RawWriteLock = new object();
        private static readonly object ColorLock = new object();

        public const ConsoleColor WarningColor = ConsoleColor.Yellow;
        public const ConsoleColor TimeColor = ConsoleColor.DarkGreen;

        private static int TimeLength;

        public static void Warn(object sender, object val)
        {
            WriteTimestamp();

            lock (ColorLock)
            {
                var oldFore = Console.ForegroundColor;
                Console.ForegroundColor = WarningColor;

                RawWrite($"[{sender}] {val}{Environment.NewLine}");

                Console.ForegroundColor = oldFore;
            }
        }

        public static void RawWrite(object val)
        {
            lock (RawWriteLock)
                Console.Write(val);
        }

        private static void WriteTimestamp()
        {
            RawWrite('[');

            lock (ColorLock)
            {
                var oldFore = Console.ForegroundColor;
                Console.ForegroundColor = TimeColor;

                var str = DateTime.Now.ToString("yy/MM/dd HH:mm:ss");

                RawWrite(str);
                TimeLength = str.Length+2;

                Console.ForegroundColor = oldFore;
            }

            RawWrite("] ");
        }

        public static void WriteLine(object val)
        {
            if(val == null || (val is string && string.IsNullOrEmpty((string)val)))
            {
                RawWrite(Environment.NewLine);
                return;
            }

            WriteTimestamp();

            RawWrite(val + Environment.NewLine);
        }

        public static void WriteLineNoTime(object val)
            => RawWrite($"{new string(' ', TimeLength)} {val}{Environment.NewLine}");
    }
}

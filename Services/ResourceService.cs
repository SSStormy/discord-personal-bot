﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;
using Discord;
using Newtonsoft.Json;

namespace tqbot.Services
{
    public sealed class ResourceService : IService
    {
        private sealed class ResourceHookData
        {
            public FieldInfo Field { get; }
            public object Reference { get; }
            public string Key { get; }

            public ResourceHookData(string key, object reference, FieldInfo field)
            {
                Key = key;
                Reference = reference;
                Field = field;
            }
        }

        private bool _autosaveActive;

        private readonly List<ResourceHookData> _hooks = new List<ResourceHookData>();

        public void Install(DiscordClient client)
        {
            Log.WriteLine("ResourceService: Install");

            Directory.CreateDirectory(Globals.Config.ResourceDir);
            Utils.GetResources = this;

            Task.Run(async () =>
            {
                if (_autosaveActive)
                    return;

                _autosaveActive = true;

                Log.WriteLine("Begin Autosave loop");

                while (true)
                {
                    await Task.Delay(Globals.Config.AutosaveInternalSeconds*1000);
                    Save();
                }
            });
        }

        private void AbstractSave(string key, object value)
        {
            Log.WriteLine("Saving resources.");

            Formatting format = Formatting.None;
            if(Utils.IsInDebug())
               format = Formatting.Indented;
            else if(Utils.IsInRelease())
                format = Formatting.None;

            var saveFile = GetSaveDirFromKey(key);
            var data = JsonConvert.SerializeObject(value, format);
            File.WriteAllText(saveFile, data);
            Log.WriteLine($"Saved {data.Length} chars to {saveFile}");
        }

        private string GetSaveDirFromKey(string key)
            => Path.Combine(Globals.Config.ResourceDir, key + ".json");

        public void AddHook(object us, string fieldName, string key)
        {
            if(_hooks.FirstOrDefault(h => h.Key.Equals(key, StringComparison.InvariantCultureIgnoreCase)) != null)
                Debug.Fail($"Duplicate resource key: {key}");

            var field =
                us.GetType()
                    .GetRuntimeFields()
                    .FirstOrDefault(f => f.Name.Equals(fieldName, StringComparison.InvariantCulture));

            Debug.Assert(field != null);

            _hooks.Add(new ResourceHookData(key, us, field));
            Log.WriteLine($"Hook [Key: {key} Field: {field.Name}]");
        }

        public void Save()
        {
            Log.WriteLine("Saving hooks.");

            foreach (var hook in _hooks)
                AbstractSave(hook.Key, hook.Field.GetValue(hook.Reference));

            Log.WriteLine($"Saved {_hooks.Count} hooks.");
        }

        public void Load()
        {
            Log.WriteLine("Loading hooks.");

            var loaded = 0;
            foreach (var hook in _hooks)
            {
                var fileDir = GetSaveDirFromKey(hook.Key);
                if (!File.Exists(fileDir))
                {
                    Log.Warn(this, $"Attempted to load hook key {hook.Key} that does not have a file at {fileDir}.");
                    continue;
                }

                loaded++;
                hook.Field.SetValue(hook.Reference, JsonConvert.DeserializeObject(File.ReadAllText(fileDir), hook.Field.FieldType));
            }

            Log.WriteLine($"Loaded {loaded} hooks.");
        }
    }
}

﻿using System;
using System.Diagnostics;
using System.Text;
using System.Threading.Tasks;
using Discord;
using Discord.Commands;
using Discord.Modules;
using tqbot.Modules;
using tqbot.Modules.Syndicator;
using tqbot.Services;

namespace tqbot
{
    public sealed class PrivateBot
    {
        public BotConfig Config { get; }

        public DiscordClient Client { get; }

        public PrivateBot(BotConfig config)
        {
            Config = config;
            Client = new DiscordClient(cf =>
            {
                cf.AppName = "Shut the fuck up";
                cf.LogHandler += LogHandler;
            });
        }


        private readonly IService[] _services =
        {
            new ResourceService(),
        };

        private readonly IModule[] _modules =
        {
            new AdminModule(),
            new TagModule(),
            new QuoteModule(),
            new SyndicatorModule(),
            new ColorModule(), 
        };

        public async Task Init()
        {
            var watch = new Stopwatch();
            Globals.Config = Config;
            watch.Start();
            Log.WriteLine("Begin init.");

            string state = null;
            string token = null;
            if (Utils.IsInDebug())
            {
                state = "DEBUG";
                Log.WriteLine("Using DEBUG token.");
                token = Config.TokenDebug;
            }
            else if (Utils.IsInRelease())
            {
                state = "RELEASE";
                Log.WriteLine("Using RELEASE token");
                token = Config.TokenRelease;
            }

            await Client.Connect(token, TokenType.Bot);
            Log.WriteLine($"Connected to Discord as: {Client.CurrentUser.Name}");

            Log.WriteLine("Begin loading built-in services");
            Client.UsingModules();
            Client.UsingCommands(cm =>
            {
                cm.AllowMentionPrefix = true;
                cm.PrefixChar = '.';
                cm.HelpMode = HelpMode.Public;
                cm.IsSelfBot = false;
                cm.ErrorHandler += async (s, e) => await CmdErrorHandler(s, e);
            });

            Log.WriteLine($"Begin loading services");
            foreach (var service in _services)
            {
                Log.WriteLine($"Service: loading {service.GetType().Name}");
                Client.AddService(service);
            }

            Log.WriteLine("Begin loading modules");

            foreach (var module in _modules)
            {
                Log.WriteLine($"Module: loading {module.GetType().Name}");
                Client.AddModule(module);
            }

            Utils.GetResources.Load();
            watch.Stop();

            Console.Clear();
            Log.WriteLine($"{state} init complete in {watch.ElapsedMilliseconds}ms.");
            Log.WriteLine($"Version {Globals.Config.Version}");

            Log.WriteLine(null);
            Log.WriteLine("Active services:");
            foreach (var service in _services)
                Log.WriteLineNoTime(service.GetType().Name);

            Log.WriteLine(null);
            Log.WriteLine("Active modules:");
            foreach (var module in _modules)
                Log.WriteLineNoTime(module.GetType().Name);

            
        }

        private async Task CmdErrorHandler(object sender, CommandErrorEventArgs e)
        {
            switch (e.ErrorType)
            {
                case CommandErrorType.Exception:
                    if (e.Exception != null && e.Exception.GetType() == typeof(Discord.Net.HttpException))
                    {
                        await Task.Delay(1000);
                        await e.Channel.SendMessage("Due to Discord being absolutely retarded, the last message was consumed by the void, never to be seen again. `(502 Bad gateway)`");
                    }
                    await e.Channel.SendMessage($"{e.User.Mention} Something went wrong while processing your command! Make sure your input is in the valid format. `({e.Exception.GetType().Name})`\r\nContact @stormy if something actually broke or something.");
                    Log.Warn("CommandService", $"Exception on command: {e.Exception}");
                    break;
                case CommandErrorType.UnknownCommand:
                    await e.Channel.SendMessage($"{e.User.Mention} that command does not exist.");
                    break;
                case CommandErrorType.BadPermissions:
                    var builder =
                        new StringBuilder($"{e.User.Mention} you do not have sufficient permissions for that command. ");
                    if (!string.IsNullOrEmpty(e.Exception?.Message))
                        builder.AppendLine($"Error message: ```{e.Exception.Message}```");
                    await e.Channel.SendMessage(builder.ToString());
                    break;
                case CommandErrorType.BadArgCount:
                    await e.Channel.SendMessage($"{e.User.Mention} bad argument count.");
                    break;
                case CommandErrorType.InvalidInput:
                    await e.Channel.SendMessage($"{e.User.Mention} invalid command input.");
                    break;
                default:
                    Log.Warn("CommandService", $"e.ErrorType ({e.ErrorType}) is not handled.");
                    break;
            }
        }

        private void LogHandler(object sender, LogMessageEventArgs e)
        {
            if (2 > (int) e.Severity)
                return;

            Log.Warn(e.Source, e.Message);

            if(e.Exception != null)
                Log.Warn(e.Source, e.Exception);
        }
    }
}

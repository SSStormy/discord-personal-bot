﻿using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Discord;
using Discord.Commands;
using Discord.Modules;

namespace tqbot.Modules
{
    public sealed class TagModule : IModule
    {
        private const string ResourceTagName = "_tags";
        private ConcurrentDictionary<string, string> _tags = new ConcurrentDictionary<string, string>();

        private DiscordClient _client;

        private readonly HashSet<string> _keyBlacklist = new HashSet<string>
        {
            "remove",
            "delete",
            "add",
            "list"
        };

        public void Install(ModuleManager manager)
        {
            Utils.GetResources.AddHook(this, "_tags", ResourceTagName);
            _client = manager.Client;
            manager.CreateCommands("tag", group =>
            {
                group.AddCheck(Globals.TqOnly);

                group.CreateCommand("")
                    .Parameter("key")
                    .Do(async e =>
                    {
                        var key = e.GetArg("key");
                        if (!Utils.IsValidParam(key))
                        {
                            await e.Channel.SendMessage($"{Globals.Rage} invalid tag key.");
                            return;
                        }
                        if (!_tags.ContainsKey(key))
                        {
                            await e.Channel.SendMessage($"{Globals.Rage} tag doesn't exist.");
                            return;
                        }
                        await e.Channel.SendMessage(_tags[key]);
                    });

                group.CreateCommand("remove")
                    .Alias("delete")
                    .Parameter("key")
                    .Do(async e =>
                    {
                        var key = e.GetArg("key");
                        if (!Utils.IsValidParam(key))
                        {
                            await e.Channel.SendMessage($"{Globals.Rage} invalid tag key.");
                            return;
                        }
                        if (!_tags.ContainsKey(key))
                        {
                            await e.Channel.SendMessage($"{Globals.Rage} tag doesn't exist.");
                            return;
                        }

                        string ignored;
                        if (!_tags.TryRemove(key, out ignored))
                            await e.Channel.SendMessage($"{Globals.Rage} something exploded. Tag not removed");
                        else
                            await e.Channel.SendMessage(Globals.OkHand);
                    });

                group.CreateCommand("add")
                    .Parameter("key")
                    .Parameter("value", ParameterType.Unparsed)
                    .Do(async e =>
                    {
                        var key = e.GetArg("key");
                        var val = e.GetArg("value");
                        if (!Utils.IsValidParam(key, _keyBlacklist))
                        {
                            await e.Channel.SendMessage($"{Globals.Rage} invalid tag key.");
                            return;
                        }
                        if (!Utils.IsValidParam(val))
                        {
                            await e.Channel.SendMessage($"{Globals.Rage} invalid tag value.");
                            return;
                        }
                        if (_tags.ContainsKey(key))
                        {
                            await e.Channel.SendMessage($"{Globals.Rage} tag exists.");
                            return;
                        }

                        if (!_tags.TryAdd(key, val))
                            await e.Channel.SendMessage($"{Globals.Rage} something exploded. Tag not added.");
                        else
                            await e.Channel.SendMessage(Globals.OkHand);
                    });

                group.CreateCommand("list")
                    .Do(async e =>
                    {
                        if (!_tags.Any())
                        {
                            await e.Channel.SendMessage($"{Globals.Rage} no tags available.");
                            return;
                        }

                        var builder = new StringBuilder("**Available tags:**\r\n");
                        for (var i = 0; i < _tags.Keys.Count; i++)
                        {
                            builder.Append(Format.Code(_tags.Keys.ElementAt(i)));
                            if (i != _tags.Keys.Count - 1) builder.Append(", ");
                        }
                        await e.Channel.SendMessage(builder.ToString());
                    });
            });
        }
    }
}
﻿using System;
using Discord.Modules;

namespace tqbot.Modules
{
    public sealed class AdminModule : IModule
    {
        public void Install(ModuleManager manager)
        {
            manager.CreateCommands("own", group =>
            {
                group.AddCheck(Globals.OwnerOnly);

                group.CreateCommand("save")
                    .Do(async e =>
                    {
                        Utils.GetResources.Save();
                        await e.Channel.SendMessage($"{Globals.OkHand} saved");
                    });
                group.CreateCommand("load")
                    .Do(async e =>
                    {
                        Utils.GetResources.Load();
                        await e.Channel.SendMessage($"{Globals.OkHand} loaded");
                    });

                group.CreateCommand("killbot")
                    .Do(async e =>
                    {
                        await e.Channel.SendMessage($"kys");
                        Utils.GetResources.Save();
                        Environment.Exit(Environment.ExitCode);
                    });
                group.CreateCommand("prune")
                    .Do(async e =>
                    {
                        foreach (var msg in await e.Channel.DownloadMessages())
                        {
                            if (msg == null)
                                continue;
                            await msg.Delete();
                        }
                        await e.Channel.SendMessage(Globals.OkHand);
                    });
            });
        }
    }
}

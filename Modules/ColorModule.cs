﻿using System;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using Discord;
using Discord.Modules;

namespace tqbot.Modules
{
    public sealed class ColorModule : IModule
    {
        private const string ColorRoleIdentifier = "Colour";

        public void Install(ModuleManager manager)
        {
            manager.CreateCommands("color", group =>
            {
                group.AddCheck(Globals.TqOnly);

                group.CreateCommand("list")
                    .Description("Lists available colors.")
                    .Do(async e =>
                    {
                        var builder = new StringBuilder("**Available color roles:**\r\n`");
                        var count = 0;

                        foreach (var role in e.Server.Roles)
                        {
                            if (!IsRoleNameAValidColor(role.Name))
                                continue;
                            builder.Append(role.Name + " ");
                            count ++;
                        }

                        if (count == 0)
                        {
                            await e.Channel.SendMessage($"No color roles available.");
                            return;
                        }
                        await e.Channel.SendMessage($"{builder}`");
                    });

                group.CreateCommand("set")
                    .Parameter("colorRole")
                    .Description("Sets your color.")
                    .Do(async e =>
                    {
                        var role = TranslateRolenameToRole(e.Server, e.GetArg("colorRole"));
                        if (role == null)
                        {
                            await e.Channel.SendMessage($"{Globals.Rage} invalid role.");
                            return;
                        }

                        await ClearColorRoles(e.User);
                        await e.User.AddRoles(role);
                        await e.Channel.SendMessage(Globals.OkHand);
                    });

                group.CreateCommand("remove")
                    .Description("Removes your color roles.")
                    .Do(async e =>
                    {
                        await ClearColorRoles(e.User);
                        await e.Channel.SendMessage(Globals.OkHand);
                    });
            });
        }

        private async Task ClearColorRoles(User user)
        {
            foreach (var role in user.Roles)
            {
                if (!IsRoleNameAValidColor(role.Name))
                    continue;

                await user.RemoveRoles(role);
            }
        }

        private bool IsRoleNameAValidColor(string rolename)
        {
            if (!rolename.StartsWith(ColorRoleIdentifier))
                return false;

            if (rolename.Length <= ColorRoleIdentifier.Length)
                return false;

            return true;
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        private Role TranslateRolenameToRole(Server server, string rolename)
            => !IsRoleNameAValidColor(rolename) 
            ? null 
            : server.Roles.FirstOrDefault(r => r.Name.Equals(rolename, StringComparison.InvariantCulture));
    }
}

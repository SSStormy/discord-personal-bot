﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Discord;
using Discord.Commands;
using Discord.Modules;

namespace tqbot.Modules.Syndicator
{
    public sealed class SyndicatorModule : IModule
    {
        private const string ResourceFeedKey = "syndication_feeds";
        private const string ResoucreChannelKey = "syndication_channel";

        private HashSet<SyndicationFeed> _feeds = new HashSet<SyndicationFeed>();
        private ulong _channelId = default(ulong);

        private bool _feedUpdLoopActive;

        private DiscordClient _client;

        private Channel GetContentChannel()
        {
            var server = _client.GetServer(Globals.Config.TqId);

            if (_channelId != default(ulong))
            {
                var chnl = server.GetChannel(_channelId);
                if (chnl != null)
                    return chnl;
            }

            _channelId = default(ulong);
            return server.DefaultChannel;
        }

        public void Install(ModuleManager manager)
        {
            _client = manager.Client;
            Utils.GetResources.AddHook(this, "_feeds", ResourceFeedKey);
            Utils.GetResources.AddHook(this, "_channelId", ResoucreChannelKey);

            Task.Run(async () =>
            {
                if (_feedUpdLoopActive)
                {
                    Log.WriteLine("Attempted to start the feed update loop when it's state is already running.");
                    return;
                }

                _feedUpdLoopActive = true;
                Log.WriteLine("Start feed update loop");

                while (true)
                {
                    await Update();
                    await Task.Delay(Globals.Config.FeedUpdateIntervalSeconds * 1000);
                }
            });

            manager.CreateCommands("rss", group =>
            {
                group.AddCheck(Globals.TqOnly);

                group.CreateCommand("channel")
                    .Description("Set the channel in which content updates will be posted.")
                    .Parameter("means_of_id-ing_the_channel", ParameterType.Unparsed)
                    .Do(async e =>
                    {
                        var channelQuery = e.GetArg("means_of_id-ing_the_channel");
                        if (string.IsNullOrEmpty(channelQuery))
                        {
                            await
                                e.Channel.SendMessage(
                                    $"Current channel: {e.Channel.SendMessage(GetContentChannel().Name)}");
                            return;
                        }

                        var channelsEnumerable = e.Server.FindChannels(channelQuery, ChannelType.Text);
                        var channels = channelsEnumerable as IList<Channel> ?? channelsEnumerable.ToList();
                        if (!channels.Any())
                        {
                            await e.Channel.SendMessage($"{Globals.Rage} channel not found.");
                            return;
                        }
                        if (channels.Count != 1)
                        {
                            await e.Channel.SendMessage($"{Globals.Rage} too many channels match that query.");
                            return;
                        }
                        var channel = channels.FirstOrDefault();
                        if (channel == null)
                        {
                            await e.Channel.SendMessage($"{Globals.Rage} matched channel was null.");
                            return;
                        }

                        _channelId = channel.Id;
                        await e.Channel.SendMessage($"{Globals.OkHand}");
                    });

                #region feed

                group.CreateCommand("add")
                    .Description("Adds an rss feed.")
                    .Parameter("feed_url")
                    .Do(async e =>
                    {
                        var feedUrl = e.GetArg("feed_url");
                        if (!Utils.IsValidParam(feedUrl))
                        {
                            await e.Channel.SendMessage($"{Globals.Rage} garbage data.");
                            return;
                        }

                        if (_feeds.FirstOrDefault(f => f.FeedUrl.Equals(feedUrl, StringComparison.InvariantCulture)) !=
                            null)
                        {
                            await e.Channel.SendMessage($"{Globals.Rage} feed already exists.");
                            return;
                        }

                        var feed = new SyndicationFeed(feedUrl);
                        _feeds.Add(feed);

                        await e.Channel.SendMessage($"{Globals.OkHand}");
                    });

                group.CreateCommand("remove")
                    .Description("Remove an rss feed.")
                    .Parameter("feed_url")
                    .Do(async e =>
                    {
                        var feed = e.GetArg("feed_url");
                        if (!Utils.IsValidParam(feed))
                        {
                            await e.Channel.SendMessage($"{Globals.Rage} garbage data.");
                            return;
                        }
                        if (_feeds.RemoveWhere(f => f.FeedUrl.Equals(feed, StringComparison.InvariantCulture)) == 0)
                        {
                            await e.Channel.SendMessage($"{Globals.Rage} feed does not exist.");
                            return;
                        }

                        await e.Channel.SendMessage($"{Globals.OkHand}");
                    });

                group.CreateCommand("List")
                    .Description("List all watched rss feeds.")
                    .Do(async e =>
                    {
                        if (!_feeds.Any())
                        {
                            await e.Channel.SendMessage($"No feeds available.");
                            return;
                        }

                        // build payload                         
                        var builder = new StringBuilder();
                        builder.AppendLine($"Active feeds:\r\n");

                        var i = 0;
                        foreach (var feed in _feeds)
                            builder.AppendLine($"- {++i}: {feed.FeedUrl}");

                        // deploy                         
                        const string quotesDir = "feeds/";
                        var callback = Utils.DeployToServer(quotesDir, e.Server.Id.ToString() + ".txt", builder.ToString());
                        await e.Channel.SendMessage(callback);
                    });


                group.CreateCommand("update")
                    .Do(async e =>
                    {
                        await Update();
                        await e.Channel.SendMessage(Globals.OkHand);
                    });

                #endregion

                #region Sub

                group.CreateCommand("subscribe")
                    .Description("Subscribe to an rss feed so you get pinged each time new content is available.")
                    .Parameter("feed_url")
                    .Do(async e =>
                    {
                        var feedUrl = e.GetArg("feed_url");
                        var feed =
                            _feeds.FirstOrDefault(f => f.FeedUrl.Equals(feedUrl, StringComparison.InvariantCulture));

                        if (feed == null)
                        {
                            await e.Channel.SendMessage($"{Globals.Rage} feed not found.");
                            return;
                        }

                        if (!feed.Subscribe(e.User))
                        {
                            await e.Channel.SendMessage($"{Globals.Rage} already subscribed.");
                            return;
                        }

                        await e.Channel.SendMessage(Globals.OkHand);
                    });
                group.CreateCommand("unsubscribe")
                    .Description("Unsubscribe from an rss feed.")
                    .Parameter("feed_url")
                    .Do(async e =>
                    {
                        var feedUrl = e.GetArg("feed_url");
                        var feed =
                            _feeds.FirstOrDefault(f => f.FeedUrl.Equals(feedUrl, StringComparison.InvariantCulture));

                        if (feed == null)
                        {
                            await e.Channel.SendMessage($"{Globals.Rage} feed not found.");
                            return;
                        }

                        if (!feed.Unsubscribe(e.User))
                        {
                            await e.Channel.SendMessage($"{Globals.Rage} not subscribed.");
                            return;
                        }

                        await e.Channel.SendMessage(Globals.OkHand);
                    });


                group.CreateCommand("list_subbed")
                    .Description("List all of your subscribed feeds (the ones you get pinged by).")
                    .Do(async e =>
                    {
                        const string header = "**Subscriptions:**\r\n";
                        var builder = new StringBuilder(header);
                        foreach (var feed in _feeds)
                        {
                            if (feed.IsSubscribed(e.User))
                                builder.AppendLine($"`- {feed.FeedUrl}`");
                        }
                        if (builder.Length <= header.Length)
                        {
                            await e.Channel.SendMessage("You are not subscribed to any feeds.");
                            return;
                        }
                        await e.Channel.SendMessage(builder.ToString());
                    });

                #endregion
            });
        }

        private bool _isUpdating;

        private async Task Update()
        {
            if (_isUpdating)
                return;

            _isUpdating = true;
            try
            {
                foreach (var results in await Task.WhenAll(from feed in _feeds
                                                           select feed.Update()))
                {
                    if (results == null || !results.Content.Any())
                        continue;

                    var channel = GetContentChannel();
                    var subs = results.Feed.GetSubsciberMentions(_client);

                    var cur = 0;
                    var builder = new StringBuilder(subs ?? "");
                    const int maxChar = 2000;
                    while (cur < results.Content.Count)
                    {
                        if (builder.Length + results.Content[cur].Message.Length + Environment.NewLine.Length <
                            maxChar)
                        {
                            builder.AppendLine(results.Content[cur].Message);
                        }
                        else
                        {
                            var message = builder.ToString();
                            await channel.SendMessage(message);
                            builder = new StringBuilder(subs ?? "");

                        }
                        cur++;
                    }
                    var nextMsg = builder.ToString();
                    if (!string.IsNullOrEmpty(nextMsg))
                        await channel.SendMessage(nextMsg);
                }
            }
            catch (Exception ex)
            {
                Log.Warn(this, $"Caught exception ({ex.GetType().Name}):\r\n        {ex}");
            }
            finally
            {
                _isUpdating = false;
            }
        }
    }
}

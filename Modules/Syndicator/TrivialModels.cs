﻿using System.Collections.Generic;
using System.Text;
using System.Text.RegularExpressions;

namespace tqbot.Modules.Syndicator
{
    public sealed class SyndicationContent
    {
        public string Header { get; }
        public string Description { get; }
        public string Url { get; }

        public string Message { get; }

        private const int DescMaxLen = 100;

        public SyndicationContent(string header, string description, string url)
        {
            Header = header;
            Description = description;
            Url = url;

            var builder = new StringBuilder($"**{Header}**\r\n");
            if (description != null)
            {
                description = Regex.Replace(description, "<[^>]+>", string.Empty);
                builder.AppendLine(description.Length >= DescMaxLen
                    ? description.Substring(0, DescMaxLen - 3) + "..."
                    : description);
            }
            builder.AppendLine(url);
            Message = builder.ToString();
        }
    }

    public sealed class UpdateResult
    {
        public List<SyndicationContent> Content { get; }
        public SyndicationFeed Feed { get; }

        public UpdateResult(List<SyndicationContent> content, SyndicationFeed feed)
        {
            Content = content;
            Feed = feed;
        }
    }
}
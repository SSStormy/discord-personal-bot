using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using Argotic.Syndication;
using Discord;
using Newtonsoft.Json;

namespace tqbot.Modules.Syndicator
{
    [JsonObject(MemberSerialization.OptIn)]
    public sealed class SyndicationFeed : IEquatable<SyndicationFeed>
    {
        public enum FeedType
        {
            Unparsable = -1,
            Unknown = 0,
            Rss,
            Atom
        }

        [JsonProperty]
        public FeedType TypeOfFeed { get; private set; } = FeedType.Unknown;

        [JsonProperty]
        public string FeedUrl { get; }

        [JsonProperty]
        private readonly HashSet<string> _seen = new HashSet<string>();
        [JsonProperty]
        private readonly HashSet<ulong> _subIds = new HashSet<ulong>();
        [JsonProperty]
        private bool IsNew { get; set; }

        private Uri _feedUri;
        private Uri FeedUrlButItsAnUri => _feedUri ?? (_feedUri = new Uri(FeedUrl));

        [JsonConstructor]
        private SyndicationFeed(string feedUrl, HashSet<ulong> subIds, bool isNew, HashSet<string> seen)
        {
            FeedUrl = feedUrl;
            _subIds = subIds;
            IsNew = isNew;
            _seen = seen;

            _subIds = subIds ?? new HashSet<ulong>();
            _seen = _seen ?? new HashSet<string>();
        }

        /// <summary>
        /// Constucts a brand new feed with the new flag set.
        /// </summary>
        public SyndicationFeed(string feedUrl)
        {
            if (feedUrl == null) throw new ArgumentNullException(nameof(feedUrl));
            FeedUrl = feedUrl;
        }

        public bool Subscribe(User user)
            => _subIds.Add(user.Id);

        public bool Unsubscribe(User user)
            => _subIds.Remove(user.Id);

        public bool IsSubscribed(User user)
            => _subIds.Contains(user.Id);

        /// <param name="client">The client with which to resolve subscriber ids.</param>
        /// <returns>Returns a formatted mention string that tags all subsctibers. Null if nobody is subscribed.</returns>
        public string GetSubsciberMentions(DiscordClient client)
        {
            if (!_subIds.Any())
                return null;

            var server = client.GetServer(Globals.Config.TqId);
            var builder = new StringBuilder();

            foreach (var id in _subIds)
            {
                var user = server.GetUser(id);
                if (user == null)
                {
                    _subIds.Remove(id);
                    continue;
                }
                builder.Append(user.Mention).Append(' ');
            }
            return builder.ToString();
        }

        public Task<UpdateResult> Update()
        {
            return Task.Run(() =>
            {
                var retval = new List<SyndicationContent>();

                if (TypeOfFeed == FeedType.Unknown)
                    TypeOfFeed = FindType();
                try
                {

                    switch (TypeOfFeed)
                    {
                        case FeedType.Rss:
                            var rss = RssFeed.Create(FeedUrlButItsAnUri);
                            foreach (var entry in rss.Channel.Items)
                            {
                                if (!_seen.Contains(entry.Guid.ToString()))
                                {
                                    _seen.Add(entry.Guid.ToString());
                                    retval.Add(new SyndicationContent(entry.Title, entry.Description,
                                        entry.Link.ToString()));
                                }
                            }
                            break;
                        case FeedType.Atom:
                            var atom = AtomFeed.Create(FeedUrlButItsAnUri);
                            foreach (var entry in atom.Entries)
                            {
                                if (!_seen.Contains(entry.Id.ToString()))
                                {
                                    if (entry.Title == null || entry.Links?.FirstOrDefault() == null) continue;

                                    _seen.Add(entry.Id.ToString());
                                    retval.Add(new SyndicationContent(entry.Title.ToString(), entry.Summary.ToString(),
                                        entry.Links.FirstOrDefault().ToString()));
                                }
                            }
                            break;
                    }

                    return new UpdateResult(retval, this);
                }
                catch (Exception ex)
                {
                    Log.Warn(GetType().Name, $"Feed {FeedUrl} threw: {ex}");
                }
                return null;
            });
        }


        private FeedType FindType()
        {
            try
            {
                if (RssFeed.Create(FeedUrlButItsAnUri) != null) return FeedType.Rss;
            }
            catch (Exception)
            {

            }
            try
            {
                if (AtomFeed.Create(FeedUrlButItsAnUri) != null) return FeedType.Atom;
            }
            catch (Exception)
            {

            }
            Log.Warn(GetType().Name, $"Unknown feed. URL: {FeedUrl}");
            return FeedType.Unparsable;
        }

        #region Equals

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        private bool InternalEquals(SyndicationFeed other)
        {
            return other != null && FeedUrl.Equals(other.FeedUrl, StringComparison.InvariantCulture);
        }

        public bool Equals(SyndicationFeed other)
        {
            return Utils.CommonEquals(this, other) || InternalEquals(other);
        }

        public override bool Equals(object other)
        {
            return Utils.CommonEquals(this, other) || InternalEquals(other as SyndicationFeed);
        }

        public override int GetHashCode()
        {
            return FeedUrl.GetHashCode()*3;
        }

        #endregion
    }
}
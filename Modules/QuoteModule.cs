﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Discord;
using Discord.Commands;
using Discord.Modules;

namespace tqbot.Modules
{
    public sealed class QuoteModule : IModule
    {
        private const string ResourceQuoteName = "quotes";

        private List<string> _quotes = new List<string>();

        private Random _rng;

        public void Install(ModuleManager manager)
        {
            _rng = new Random();
            Utils.GetResources.AddHook(this, "_quotes", ResourceQuoteName);

            manager.CreateCommands("quote", group =>
            {
                group.AddCheck(Globals.TqOnly);

                group.CreateCommand("")
                    .Do(async e =>
                    {
                        if (!_quotes.Any())
                        {
                            await e.Channel.SendMessage($"{Globals.Rage} no quotes available.");
                            return;
                        }

                        await e.Channel.SendMessage(_quotes[_rng.Next(0, _quotes.Count)]);
                    });

                group.CreateCommand("add")
                    .Parameter("quote", ParameterType.Unparsed)
                    .Description("Adds a quote to the quotes list.")
                    .Do(async e =>
                    {
                        _quotes.Add(e.GetArg("quote"));
                        await e.Channel.SendMessage(Globals.OkHand);
                    });

                group.CreateCommand("remove")
                    .Parameter("index")
                    .Description("Removed a quote found at the given index.")
                    .Do(async e =>
                    {
                        int index = int.Parse(e.GetArg("index")) - 1;
                        if (0 > index || index >= _quotes.Count)
                        {
                            await e.Channel.SendMessage($"{Globals.Rage} Index out of range.");
                            return;
                        }
                        string quote = _quotes[index];
                        _quotes.RemoveAt(index);
                        await e.Channel.SendMessage($"{Globals.OkHand} Removed quote: `{quote}`");
                    });

                group.CreateCommand("list").Description("Lists all available quotes with their indexes.").Do(async e =>
                {
                    if (!_quotes.Any())
                    {
                        await e.Channel.SendMessage("No quotes available.");
                        return;
                    }
                    // build payload                         
                    StringBuilder builder = new StringBuilder();
                    builder.AppendLine($"Active quotes:\r\n");
                    for (int i = 0; i < _quotes.Count; i++)
                    {
                        builder.AppendLine($"- {i + 1}: {_quotes[i]}");
                    }
                    // deploy                         
                    const string quotesDir = "quotes/";
                    var callback = Utils.DeployToServer(quotesDir, e.Server.Id.ToString() + ".txt", builder.ToString());
                    await e.Channel.SendMessage(callback);
                });
            });
        }
    }
}

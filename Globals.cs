﻿using System;
using Discord;
using Discord.Commands;

namespace tqbot
{
    public static class Globals
    {
        private static BotConfig _config;

        public static BotConfig Config
        {
            get { return _config; }
            set
            {
                if(_config != null)
                    throw new InvalidOperationException("Attempted to overwrite BotConfig value that was already set in Globals. Are you running two instances of PrivateBot?");
                _config = value;
            }
        }

        public static bool TqOnly(Command cmd, User usr, Channel chnl)
        {
            if (Utils.IsInDebug())
                return true;

            if (chnl == null || usr == null || cmd == null || chnl.IsPrivate || chnl.Server == null)
                return false;
            return chnl.Server.Id == Globals.Config.TqId;
        }

        public static bool OwnerOnly(Command cmd, User usr, Channel chnl)
        {
            if (usr == null)
                return false;

            return usr.Id == Globals.Config.OwnerId;
        }

        public static string[] LaunchArgs { get; set; }

        public const string OkHand = "👌";
        public const string Rage = "😡";
    }
}

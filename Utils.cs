﻿using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Runtime.CompilerServices;
using Discord;
using tqbot.Services;

namespace tqbot
{
    public static class Utils
    {
        public static ResourceService GetResources { get; set; }

        public static bool IsValidParam(string value, ICollection<string> blacklist = null, bool caseSensitive = true)
        {
            if (!caseSensitive)
                value = value.ToLowerInvariant();

            if (string.IsNullOrEmpty(value))
                return false;

            if (blacklist != null && blacklist.Contains(value))
                return false;

            return true;
        }

        public static string DeployToServer(string dir, string filename, string payload)
        {
            var absDir = Path.Combine(Globals.Config.ServerDeployDir, dir);
            Directory.CreateDirectory(absDir);

            var absFileDir = Path.Combine(absDir, filename);
            File.WriteAllText(absFileDir, payload);
            return Globals.Config.ServerDomain + Path.Combine(dir, filename);
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public static bool CommonEquals<T>(T us, object other)
        {
            return other != null && ReferenceEquals(us, other);
        }

        public static bool IsInDebug()
        {

            if (Globals.LaunchArgs.Contains("--debug"))
                return true;

            if (IsInRelease())
                return false;
#if DEBUG
            return true;
#elif RELEASE
            return false;
#endif
            Debug.Fail("Unknown debug state.");
        }

        public static bool IsInRelease()
        {
            if (Globals.LaunchArgs.Contains("--release"))
                return true;
#if RELEASE
            return true;
#elif DEBUG
            return false;
#endif

            Debug.Fail("Unknown release state.");
        }
    }
}

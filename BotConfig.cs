﻿using System;

namespace tqbot
{
    public sealed class BotConfig
    {
        public string Version { get; }

        public string TokenDebug { get; }
        public string TokenRelease { get; }

        public int FeedUpdateIntervalSeconds { get; }
        public int AutosaveInternalSeconds { get; }

        public string ResourceDir { get; }

        public ulong TqId { get; }
        public ulong OwnerId { get; }

        public string ServerDeployDir { get; }
        public string ServerDomain { get; }

        public BotConfig(string tokenDebug, int autosaveInternalSeconds, string resourceDir,
            ulong tqId, ulong ownerId, string serverDeployDir, string serverDomain, 
            int feedUpdateIntervalSeconds, string tokenRelease, string version, ulong debugTqId)
        {
            if (tokenDebug == null) throw new ArgumentNullException(nameof(tokenDebug));
            if (resourceDir == null) throw new ArgumentNullException(nameof(resourceDir));
            if (tokenRelease == null) throw new ArgumentNullException(nameof(tokenRelease));
            if (version == null) throw new ArgumentNullException(nameof(version));
            if (autosaveInternalSeconds <= 0) throw new ArgumentOutOfRangeException(nameof(autosaveInternalSeconds));

            TokenDebug = tokenDebug;
            AutosaveInternalSeconds = autosaveInternalSeconds;
            ResourceDir = resourceDir;
            TqId = tqId;
            OwnerId = ownerId;
            ServerDeployDir = serverDeployDir;
            ServerDomain = serverDomain;
            FeedUpdateIntervalSeconds = feedUpdateIntervalSeconds;
            TokenRelease = tokenRelease;
            Version = version;

            if(Utils.IsInDebug())
                TqId = debugTqId;
        }
    }
}

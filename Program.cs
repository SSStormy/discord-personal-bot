﻿using System;
using System.IO;
using Newtonsoft.Json;
using Nito.AsyncEx;

namespace tqbot
{
    class Program
    {
        static void Main(string[] args)
        {
            Globals.LaunchArgs = args;

            var config = JsonConvert.DeserializeObject<BotConfig>(File.ReadAllText("config.json"));
            var bot = new PrivateBot(config);

            AsyncContext.Run(async () => await bot.Init());

            while (true)
                Console.ReadLine();
        }
    }
}
